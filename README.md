# Protech API

Ce project a été developpé avec [FastAPI](https://fastapi.tiangolo.com/) version 0.6.


## Installation

executez `git clone https://gitlab.com/protech-team/protech-api.git` pour cloner ce projet chez vous et le modifier a votre propre usage.

## Dependences
pour que ce projet fonctionne correctement il vous faudra installer quelques packages python indispensables:
- `pip install fastapi[all]` pour installer fastapi et toutes ses dependences
- `pip install ujson`
- `pip install requests`
- `pip install aiofiles`
- `pip install jinja2`
- `pip install python-multipart`
- `pip install itsdangerous`
- `pip install pyyaml`

## Serveur de developpement

executez `uvicorn main:app --reload` dans le dossier où se trouve le fichier `main.py` pour lancer le serveur de developpement. Ensuite ovrez votre navigateur a l'addresse `http://localhost:8000/`. l'application se recharge automatiquement a chaque modification du code source.

## Documentation integrée

retrouver la doc integrée en ouvra,t l'addresse `http://localhost:8000/docs`

## Plus d'Aide

Vous pouvez recevoir plus d'aide en ecrivant a ces addresses mail 
- danielgolo82@gmail.com
- hlabsdevs@gmail.com
