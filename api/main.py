from api.models import Candidature
from fastapi.encoders import jsonable_encoder
import ast
from starlette.responses import RedirectResponse
import uvicorn
from pydantic import BaseModel, Field
from passlib.context import CryptContext
from jose import JWTError, jwt
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi import Depends, FastAPI, HTTPException, status, Request, Response
from typing import Optional
from datetime import datetime, timedelta, date
from typing import List
from fastapi.params import Body, Path, Query
import time

# from .routers import entreprises, consultants
from . import crud, database, models, schemas
from .database import db_state_default

database.db.connect()
database.db.create_tables([
    models.User,
    models.Categorie,
    models.Projet,
    models.Candidature,
    models.Tache,
    models.Phase,
    models.Historique,
    models.Fichier,
    models.Commentaire,
    models.Projet.intervenants.get_through_model()
])
database.db.close()

app = FastAPI(title="ProtechAPI", debug=True,
              description="Backend du projet Protech", )

# to get a string like this run:
# openssl rand -hex 32
SECRET_KEY = "0362efedc2e60adabeb0bc77f0a632ca71041733b46738f7eaf48968fcc8f681"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 120

sleep_time = 10


async def reset_db_state():
    database.db._state._state.set(db_state_default.copy())
    database.db._state.reset()


# Dependency
def get_db(db_state=Depends(reset_db_state)):
    try:
        database.db.connect()
        yield
    finally:
        if not database.db.is_closed():
            database.db.close()


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def verify_password(plain_password, hashed_password):  # password verif
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):  # Hasher le mot de passe
    return pwd_context.hash(password)


def get_user(username: str):
    db_user = crud.get_user_by_username(username)
    print(db_user)
    if username in db_user.username:
        # user_dict = db_user[username]
        # return models.User(**user_dict)
        # return models.User(**db_user)
        return db_user


def authenticate_user(username: str, password: str):
    user = get_user(username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


# recuperer un User grace a son Token
async def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = get_user(token_data.username)
    if user is None:
        raise credentials_exception
    return user


async def get_current_active_user(current_user: schemas.User = Depends(get_current_user)):
    if not current_user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user

# Route methods start


@app.get("/", tags=["Doc"])
def main():
    return RedirectResponse(url="/docs/")


@app.get("/doc", tags=["Doc"])
def main():
    return RedirectResponse(url="/docs/")


@app.get("/doc2", tags=["Doc"])
def main():
    return RedirectResponse(url="/redoc/")


descp = """ Mettre a 1 si vous voulez suprimmer les anciennes competences deja disponible.
Mettre à 0 pour ajouter les nouvelles competences saisies a ceux qui existent deja en Base de donné
"""

# generate Token


@app.post("/token", response_model=Token, tags=["Consultant", "Entreprise"], dependencies=[Depends(get_db)])
def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(
        form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


# ================== Entreprise start==================
@app.post("/entreprises/", response_model=schemas.Entreprise, tags=["Entreprise"], dependencies=[Depends(get_db)])
def create_user_entreprise(user: schemas.EntrepriseCreate = Body(..., embed=True)):
    db_user = crud.get_user_by_email(email=user.email)
    db_user1 = crud.get_user_by_username(username=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    if db_user1:
        raise HTTPException(
            status_code=400, detail="Ce nom d'utilisateur existe deja")
    return crud.create_entreprise(user)


@app.get("/entreprises/me/", response_model=schemas.Entreprise, tags=["Entreprise"], dependencies=[Depends(get_db)])
def get_users_entreprise_courant(current_user: schemas.Entreprise = Depends(get_current_active_user)):
    if current_user.role != "Entreprise":
        raise HTTPException(
            status_code=404, detail="Vous ne pouvez pas avoir accès aux données d'un compte `Entreprise` en étant connecté en temps que`Consultant`! Connectez-vous en temps que ``Entreprise`` d'abord!")
    return current_user


@app.get("/entreprises/", response_model=List[schemas.Entreprise], tags=["Entreprise"], dependencies=[Depends(get_db)])
def get_users_entreprises(auth_user: schemas.User = Depends(get_current_active_user)):
    users = crud.get_entreprises()
    return users


@app.get("/entreprises/{user_id}", response_model=schemas.Entreprise, tags=["Entreprise"], dependencies=[Depends(get_db)])
def get_user_entreprise_par_id(user_id: int):
    db_user = crud.get_entreprise(user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.put("/entreprises/{id}", response_model=schemas.Entreprise, tags=["Entreprise"], dependencies=[Depends(get_db)])
def update_entreprises(id: str, entreprise: schemas.User):
    """
    Supporte la mise a jour partiel. C'est a dire que vous n'etes pas obligé de renseigner tout les champs.
    Mettez a jour juste les données a modifier et ignorer les autres va parfaitement marcher
    """
    stored_data = jsonable_encoder(crud.get_entreprise(id))
    if stored_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    sotred_model = schemas.User(**stored_data["__data__"])
    updated_data = entreprise.dict(
        exclude_unset=True, exclude_none=True, exclude_defaults=True)
    updated_user = sotred_model.copy(update=updated_data)
    return crud.update_user(id, updated_user, False)
# ================== Entreprise end ==================


# ================== Consultant start ==================
@app.post("/consultants/", response_model=schemas.Consultant, tags=["Consultant"], dependencies=[Depends(get_db)])
async def create_user_consultant(user: schemas.ConsultantCreate = Body(..., embed=True)):
    db_user = crud.get_user_by_email(email=user.email)
    db_user1 = crud.get_user_by_username(username=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    if db_user1:
        raise HTTPException(
            status_code=400, detail="Ce nom d'utilisateur existe deja")
    return crud.create_consultant(user)


@app.get("/consultants/me/", response_model=schemas.Consultant, tags=["Consultant"], dependencies=[Depends(get_db)])
async def get_users_consultant_courant(current_user: schemas.Consultant = Depends(get_current_active_user)):
    if current_user.role != "Consultant":
        raise HTTPException(
            status_code=404, detail="Vous ne pouvez pas avoir accès aux données d'un compte `Consultant` en étant connecté en temps qu'Entreprise! Connectez-vous en temps que Consultant d'abord!")
    return current_user


@app.get("/consultants/", response_model=List[schemas.Consultant], tags=["Consultant"], dependencies=[Depends(get_db)])
def get_users_consultants():
    users = crud.get_consultants()
    return users


@app.get("/conslutants/{user_id}", response_model=schemas.Consultant, tags=["Consultant"], dependencies=[Depends(get_db)])
def get_user_consultant_par_id(user_id: int):
    db_user = crud.get_consultant(user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.put("/consultants/{id}", response_model=schemas.Consultant, tags=["Consultant"], dependencies=[Depends(get_db)])
def update_consultant(id: int, cons: schemas.User, delet: Optional[bool] = Query(None, description=descp)):
    """
    Supporte la mise a jour partiel. C'est a dire que vous n'etes pas obligé de renseigner tout les champs.
    Mettez a jour juste les données a modifier et ignorer les autres va parfaitement marcher
    """
    stored_data = jsonable_encoder(crud.get_conslutant(id))
    if stored_data is None:
        raise HTTPException(status_code=404, detail="User not found")
    sotred_model = schemas.User(**stored_data["__data__"])
    updated_data = cons.dict(
        exclude_unset=True, exclude_none=True, exclude_defaults=True)
    updated_user = sotred_model.copy(update=updated_data)
    return crud.update_user(id, updated_user, delet)
# ================== Consultant end ==================


# ================== Projet start ==================
@app.post("/entreprise/me/offres/", response_model=schemas.Projet, tags=["Projet", "Entreprise"], dependencies=[Depends(get_db)])
def create_offre_for_user_entreprise(
    current_user: schemas.Entreprise = Depends(get_current_active_user), offre: schemas.ProjetCreate = Body(..., embed=True)
):
    """Permet a l'entreprise authentifié de creer une Offre(Projet)
    """
    if current_user.role != "Entreprise":
        raise HTTPException(
            status_code=404, detail="Seuls les comptes `Entreprise` peuvent creer un Projet!")
    return crud.create_entreprise_offre(offre, current_user.id)


@app.post("/entreprises/me/offres/{projet_id}", dependencies=[Depends(get_db)], tags=["Projet"])
def nommer_chef_projet(projet_id: int, chef_id: int, current_user: schemas.Entreprise = Depends(get_current_active_user)):
    return crud.update_chef_projet(projet_id, chef_id)


@app.get("/projets/", response_model=List[schemas.Projet], tags=["Projet"], dependencies=[Depends(get_db)])
def get_projets():
    projets = crud.get_projets()
    return projets


@app.get("/entreprises/me/offres/", response_model=List[schemas.Projet], dependencies=[Depends(get_db)], tags=["Entreprise", "Projet"])
def get_users_entreprise_offres(current_user: schemas.Entreprise = Depends(get_current_active_user)):
    return list(current_user.offres)


@app.get("/entreprises/{id}/offres/", response_model=List[schemas.Projet], dependencies=[Depends(get_db)], tags=["Entreprise", "Projet"])
def get_users_entreprises_par_id_offre(id: int, ):
    user = crud.get_entreprise(id)
    if not user:
        raise HTTPException(
            status_code=404, detail="Cet Entreprise n'existe pas!")
    return list(user.offres)


@app.get("/consultants/me/projets/", response_model=List[schemas.Projet], tags=["Projet", "Consultant"], dependencies=[Depends(get_db)])
def get_consultants_mes_projets(current_user: schemas.Consultant = Depends(get_current_active_user), ):
    return crud.get_consultant_mes_projets(current_user.id, )
# ================== Projet end ==================


# ================== Candidature start ==================
@ app.post("/consultants/me/projets/{projet_id}candidature/", response_model=schemas.Candidature, tags=["Candidature", "Consultant"], dependencies=[Depends(get_db)])
def create_offre_for_user_entreprise(projet_id: int,
                                     current_user: schemas.Consultant = Depends(get_current_active_user), candidature: schemas.CandidatureCreate = Body(..., embed=True)
                                     ):
    """Permet au Consultant authentifié de poster sa Candidature dans le but de travailler sur un projet
    """
    if current_user.role != "Consultant":
        raise HTTPException(
            status_code=404, detail="Seuls les comptes `Consultant` peuvent creer une Candidature!")
    return crud.create_candidature(candidature, projet_id=projet_id, user_id=current_user.id)


@app.get("/entreprises/me/offres/{projet_id}/candidatures/", response_model=List[schemas.Candidature], tags=["Candidature", "Projet"], dependencies=[Depends(get_db)])
def get_entreprises_candidatures_pour_mes_offres(projet_id: int,  current_user: schemas.Consultant = Depends(get_current_active_user)):
    cand = crud.get_projet(projet_id)
    if not cand:
        raise HTTPException(
            status_code=404, detail="Ce Projet n'a apparemment pas de Candidature!")
    print(f"\n{cand}\n")
    return list(cand.candidats)
# ================== Candidature end ==================


if __name__ == "__main__":
    uvicorn.run(app, host="::1", port=1200)
