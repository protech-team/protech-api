
from typing import List, Optional
from . import models, schemas
from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


# ================== Users start ==================
def create_entreprise(user: schemas.EntrepriseCreate):
    password = pwd_context.hash(user.password)
    db_user = models.User(
        email=user.email,
        username=user.username,
        hashed_password=password,
        nom=user.nom,
        role="Entreprise",
        competences=[],
        avatar=user.avatar
    )
    db_user.save()
    return db_user


def create_consultant(user: schemas.ConsultantCreate):
    password = pwd_context.hash(user.password)
    db_user = models.User(
        email=user.email,
        username=user.username,
        hashed_password=password,
        nom=user.nom,
        competences=user.competences,
        role="Consultant",
        avatar=user.avatar
    )
    db_user.save()
    return db_user


def get_all_users():
    return list(models.User.select())


def get_user(user_id: int):
    return models.User.get(models.User.id == user_id)


def get_user_by_email(email: str):
    return models.User.filter(models.User.email == email).first()


def get_user_by_username(username: str):
    return models.User.filter(models.User.username == username).first()


def get_entreprises():
    return list(models.User.select().filter(models.User.role == "Entreprise"))


def get_consultants():
    return list(models.User.select().filter(models.User.role == "Consultant"))


def get_entreprise(user_id: int):
    return models.User.filter(models.User.id == user_id, models.User.role == "Entreprise").first()


def get_consultant(user_id: int):
    return models.User.filter(models.User.id == user_id, models.User.role == "Consultant").first()
    # return models.User.get(models.User.id == user_id, models.User.role == "Consultant")


def update_user(user_id: int, ent: schemas.User, dele: Optional[bool] = False):
    saved = get_user(user_id)
    # compets = List[str]
    if dele:
        compets = ent.competences
    else:
        compets = saved.competences + ent.competences

    q = (models.User.update({
        models.User.email: ent.email,
        models.User.username: ent.username,
        models.User.nom: ent.nom,
        models.User.competences: compets,
        models.User.avatar: ent.avatar,
        models.User.is_admin: ent.is_admin,
        models.User.is_active: ent.is_active,
    }).where(models.User.id == user_id))
    nb_row = q.execute()
    return get_user(user_id)


def delete_user(user_id: int):
    return models.User.delete()
# ================== Users end ==================


# ================== Projet start ==================
def create_entreprise_offre(offre: schemas.ProjetCreate, user_id: int):
    db_offre = models.Projet(**offre.dict(), owner_id=user_id)
    db_offre.save()
    return db_offre


def get_projets():
    # return models.Projet.select()
    return list(models.Projet.select())


def get_projet(projet_id: int):
    return models.Projet.filter(models.Projet.id == projet_id).first()


def get_consultant_projets_diriges(user_id: int, ):
    return list(models.Projet.select().where(models.Projet.chef_projet_id == user_id))


def get_consultant_mes_projets(user_id: int, ):
    query = (models.Projet.select()
             .join(models.Candidature)
             .join(models.User)
             .where(models.Candidature.valide == True)
             #  .where({models.User.id == models.Candidature.candidat_id, models.Candidature.valide == True})
             .order_by(models.Projet.titre)
             .distinct())
    return list(query)


def update_chef_projet(projet_id: int, user_id: int):
    q = (models.Projet.update({models.Projet.chef_projet_id: user_id}).where(
        models.Projet.id == projet_id))
    q.execute()
    return {"message": "Succes", "details": "Chef de Projet Mise a jour avec succes"}


def update_projet(projet_id: int, ent: schemas.ProjetUpdate, ajout: Optional[bool] = False):
    # intervenants = None
    # for id_ent in ent.intervenants:
    #     conslt = get_consultant(id_ent)
    #     intervenants.add(conslt)
    # compets = List[str]
    # if ajout:

    #     intervenants = models.Projet.intervenants.add(ent.intervenants)
    # else:
    #     compets = saved.competences + ent.competences

    q = (models.Projet.update({
        models.Projet.chef_projet_id: ent.chef_projet_id,
        models.Projet.categorie_id: ent.categorie_id,
        models.Projet.tache_id: ent.tache_id,
        models.Projet.date_fin_effective: ent.date_fin_effective,
        # models.Projet.intervenants.add(intervenants),
        models.Projet.status: ent.status,
        models.Projet.suspendu: ent.suspendu,
        models.Projet.visible: ent.visible,
        models.Projet.valide: ent.valide,
    }).where(models.Projet.id == projet_id))
    nb_row = q.execute()
    return get_projet(projet_id)


# ==================  Projet end  ==================


# ================== Candidature start ==================
def get_candidatures(skip: int, limit: int):
    return list(models.Candidature.select())


def valider_consultant_candidature(projet_id: int, user_id: int):
    q = (models.Candidature.update({models.Candidature.valide: True})
         .where({
             models.Candidature.candidat_id == user_id,
             models.Candidature.projet_id == projet_id,
         })
         )
    q.execute()
    return {"message": "Succes", "details": "Candidature validée!"}


def create_candidature(candidature: schemas.CandidatureCreate, projet_id, user_id: int):
    candidat_db = models.Candidature(
        **candidature.dict(), projet_id=projet_id, candidat_id=user_id)
    candidat_db.save()
    return candidat_db
# ==================  Candidature end  ==================
