from operator import index
from playhouse.postgres_ext import ArrayField
from .database import db
from peewee import *
from datetime import datetime, date, timedelta


class User(Model):
    email = CharField(unique=True, index=True)
    username = CharField(unique=True, index=True)
    hashed_password = CharField()
    nom = CharField(unique=True, index=True)
    role = CharField(index=True)
    competences = ArrayField(CharField)
    avatar = CharField(index=True, null=True)
    is_admin = BooleanField(default=False)
    is_active = BooleanField(default=True)

    class Meta:
        database = db
        table_name = "user"


class Categorie(Model):
    nom = CharField(unique=True, index=True)
    image_url = CharField(index=True, null=True)

    class Meta:
        database = db
        table_name = "categorie"


class Projet(Model):
    titre = CharField(index=True)
    presantation = CharField(index=True)
    description = CharField(index=True)
    date_debut_prevue = DateField(index=True)
    date_fin_prevue = DateField(index=True)
    date_fin_effective = DateField(index=True, null=True)
    status = CharField(index=True, null=True)
    suspendu = BooleanField(default=False)
    visible = BooleanField(default=True)
    valide = BooleanField(default=False)

    owner = ForeignKeyField(User, backref="offres", null=True)
    chef_projet = ForeignKeyField(
        User, backref="projet_diriges", null=True)
    intervenants = ManyToManyField(
        User, backref="mes_projets")
    categorie = ForeignKeyField(
        Categorie, backref="projets", null=True)

    class Meta:
        database = db
        table_name = "projet"


class Candidature(Model):
    message = CharField(index=True)
    valide = BooleanField(default=False, null=True)
    candidat = ForeignKeyField(
        User, backref="candidatures", null=True)
    projet = ForeignKeyField(
        Projet, backref="candidats", null=True)

    class Meta:
        database = db
        table_name = "candidature"


class Tache(Model):
    titre = CharField(index=True)
    description = CharField(index=True)
    status = CharField(index=True, null=True)
    date_limite = DateField(index=True)
    date_fin_effective = DateField(index=True, null=True)
    valide = BooleanField(default=False)
    suspendue = BooleanField(default=False)

    projet = ForeignKeyField(Projet, backref="taches", null=True)
    executant = ForeignKeyField(User, backref="taches", null=True)

    class Meta:
        database = db
        table_name = "tache"


class Phase(Model):
    libelle = CharField(index=True)
    tache = ForeignKeyField(Tache, backref="phases", null=True)

    class Meta:
        database = db
        table_name = "phase"


class Historique(Model):
    date_ajout = DateField(index=True)
    libelle = CharField(index=True)
    projet = ForeignKeyField(
        Projet, backref="historiques", null=True)

    class Meta:
        database = db
        table_name = "historique"


class Fichier(Model):
    date_ajout = DateField(index=True)
    nom = CharField(index=True)
    url = CharField(index=True, null=True)
    projet = ForeignKeyField(Projet, backref="fichiers", null=True)

    class Meta:
        database = db
        table_name = "fichier"


class Commentaire(Model):
    contenue = CharField(index=True)
    user = ForeignKeyField(User, null=True)
    projet = ForeignKeyField(Projet, backref="comments", null=True)

    class Meta:
        database = db
        table_name = "commentaire"


""" 
Tables avec supression logique(A venir dans la prochaine version de la plateforme)
from operator import index
# from playhouse.postgres_ext import ArrayField
# from .database import db
# from peewee import *
# from datetime import datetime, date, timedelta


# class User(Model):
#     created_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     updated_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted = BooleanField(default=False, null=True)

#     email = CharField(unique=True, index=True)
#     username = CharField(unique=True, index=True)
#     hashed_password = CharField()
#     nom = CharField(unique=True, index=True)
#     role = CharField(index=True)
#     competences = ArrayField(CharField)
#     avatar = CharField(index=True, null=True)
#     is_admin = BooleanField(default=False)
#     is_active = BooleanField(default=True)

#     class Meta:
#         database = db
#         table_name = "users"


# class Categorie(Model):
#     created_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     updated_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted = BooleanField(default=False, null=True)

#     nom = CharField(unique=True, index=True)
#     image_url = CharField(index=True, null=True)

#     class Meta:
#         database = db
#         table_name = "categories"


# class Projet(Model):
#     created_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     updated_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted = BooleanField(default=False, null=True)

#     titre = CharField(index=True)
#     presantation = CharField(index=True)
#     description = CharField(index=True)
#     date_debut_prevue = DateField(index=True)
#     date_fin_prevue = DateField(index=True)
#     date_fin_effective = DateField(index=True, null=True)
#     status = CharField(index=True, null=True)
#     suspendu = BooleanField(default=False)
#     visible = BooleanField(default=True)
#     valide = BooleanField(default=False)

#     owner = ForeignKeyField(User, backref="offres", null=True)
#     chef_projet = ForeignKeyField(
#         User, backref="projet_diriges", null=True)
#     categorie = ForeignKeyField(
#         Categorie, backref="projets", null=True)

#     class Meta:
#         database = db
#         table_name = "projets"


# class Candidature(Model):
#     created_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     updated_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted = BooleanField(default=False, null=True)

#     message = CharField(index=True)
#     valide = BooleanField(default=False, null=True)
#     candidat = ForeignKeyField(
#         User, backref="candidatures", null=True)
#     projet = ForeignKeyField(
#         Projet, backref="candidats", null=True)

#     class Meta:
#         database = db
#         table_name = "candidatures"


# class Tache(Model):
#     created_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     updated_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted = BooleanField(default=False, null=True)

#     titre = CharField(index=True)
#     description = CharField(index=True)
#     status = CharField(index=True, null=True)
#     date_limite = DateField(index=True)
#     date_fin_effective = DateField(index=True, null=True)
#     valide = BooleanField(default=False)
#     suspendue = BooleanField(default=False)

#     projet = ForeignKeyField(Projet, backref="taches", null=True)
#     executant = ForeignKeyField(User, backref="taches", null=True)

#     class Meta:
#         database = db
#         table_name = "taches"


# class Phase(Model):
#     created_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     updated_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted = BooleanField(default=False, null=True)

#     libelle = CharField(index=True)
#     tache = ForeignKeyField(Tache, backref="phases", null=True)

#     class Meta:
#         database = db
#         table_name = "phases"


# class Historique(Model):
#     created_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     updated_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted = BooleanField(default=False, null=True)

#     date_ajout = DateField(index=True)
#     libelle = CharField(index=True)
#     projet = ForeignKeyField(
#         Projet, backref="historiques", null=True)

#     class Meta:
#         database = db
#         table_name = "historiques"


# class Fichier(Model):
#     created_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     updated_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted = BooleanField(default=False, null=True)

#     date_ajout = DateField(index=True)
#     nom = CharField(index=True)
#     url = CharField(index=True, null=True)
#     projet = ForeignKeyField(Projet, backref="fichiers", null=True)

#     class Meta:
#         database = db
#         table_name = "fichiers"


# class Commentaire(Model):
#     created_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     updated_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted_at = DateTimeField(default=datetime.now(), null=True, index=True)
#     deleted = BooleanField(default=False, null=True)

#     contenue = CharField(index=True)
#     user = ForeignKeyField(User, null=True)
#     projet = ForeignKeyField(Projet, backref="comments", null=True)

#     class Meta:
#         database = db
#         table_name = "commentaires"
 """
