from datetime import datetime, time, date, timedelta
from pydantic import BaseModel

from typing import Any, List, Optional

from peewee import ModelSelect
from pydantic.utils import GetterDict


class PeeweeGetterDict(GetterDict):  # Necessaire seulement quand peewee est utilisé
    """ Permet a Peewee de pouvoir gerer les données de type `list()` et `List[]` """

    def get(self, key: Any, default: Any = None):
        res = getattr(self._obj, key, default)
        if isinstance(res, ModelSelect):
            return list(res)
        return res


# ============ Historique classes start ============ #
class HistoriqueBase(BaseModel):
    date_ajout: Optional[date] = None
    libelle: Optional[str] = None


class HistoriqueCreate(HistoriqueBase):
    date_ajout: date = datetime.now()
    libelle: str

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict


class Historique(HistoriqueBase):
    id: Optional[int] = None
    projet_id: Optional[int] = None

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict  # seulement necessaire avec peewee
#============ Historique classes end ============#

#============ Fichier classes start ============#


class FichierBase(BaseModel):
    date_ajout: Optional[date] = None
    nom: Optional[str] = None
    url: Optional[str] = None


class FichierCreate(FichierBase):
    date_ajout: date = datetime.now()
    nom: str
    url: str

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict


class Fichier(FichierBase):
    id: Optional[int] = None
    projet_id: Optional[int] = None

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict  # seulement necessaire avec peewee
#============ Fichier classes end ============#

#============ Commentaire classes start ============#


class CommentaireBase(BaseModel):
    contenue: Optional[str] = None


class CommentaireCreate(CommentaireBase):
    contenue: str

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict


class Commentaire(CommentaireBase):
    id: Optional[int] = None
    user_id: Optional[int] = None
    projet_id: Optional[int] = None

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict  # seulement necessaire avec peewee
#============ Commentaire classes end ============#

#============ Phase classes start ============#


class PhaseBase(BaseModel):
    libelle: Optional[str] = None


class PhaseCreate(PhaseBase):
    libelle: str

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict


class Phase(PhaseBase):
    id: Optional[int] = None
    tache_id: Optional[int] = None

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict  # seulement necessaire avec peewee
#============ Phase classes end ============#

#============ Tache classes start ============#


class TacheBase(BaseModel):
    titre: Optional[str] = None
    description: Optional[str] = None
    date_limite: Optional[date] = None


class TacheCreate(TacheBase):
    titre: str
    description: str
    date_limite: date

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict


class Tache(TacheBase):
    id: Optional[int] = None
    projet_id: Optional[int] = None
    executant_id: Optional[int] = None
    phases: List[Phase] = []
    status: Optional[str] = None
    date_fin_effective: Optional[date] = None
    valide: Optional[bool] = None
    suspendue: Optional[bool] = None

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict  # seulement necessaire avec peewee

#============ Tache classes end ============#

#============ Candidature classes start ============#


class CandidatureBase(BaseModel):
    message: Optional[str] = None


class CandidatureCreate(CandidatureBase):
    message: str

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict


class Candidature(CandidatureBase):
    id: Optional[int] = None
    projet_id: Optional[int] = None
    candidat_id: Optional[int] = None
    valide: Optional[bool] = None

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict  # seulement necessaire avec peewee
# ============ Candidature classes end ============#


# ============ Projet classes start ============#
class ProjetBase(BaseModel):
    titre: Optional[str] = None
    presantation: Optional[str] = None
    description: Optional[str] = None
    date_debut_prevue: Optional[date] = None
    date_fin_prevue: Optional[date] = None


class ProjetCreate(ProjetBase):
    titre: str
    presantation: str
    description: str
    date_debut_prevue: date
    date_fin_prevue: date

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict


class ProjetUpdate(ProjetBase):
    chef_projet_id: Optional[int] = None
    categorie_id: Optional[int] = None
    tache_id: Optional[int] = None
    date_fin_effective: Optional[date] = None
    status: Optional[str] = None
    intervenants: Optional[int] = None
    suspendu: Optional[bool] = None
    visible: Optional[bool] = None
    valide: Optional[bool] = None

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict


class Intervenant(BaseModel):
    # id: int
    email: Optional[str] = None
    username: Optional[str] = None
    nom: Optional[str] = None
    avatar: Optional[str] = None  # url
    competences: Optional[List[str]] = None

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict


class Projet(ProjetBase):
    id: int
    owner_id: int
    owner: Optional[Intervenant] = None
    chef_projet_id: Optional[int]
    chef_projet: Optional[Intervenant] = None
    categorie_id: Optional[int]
    tache_id: Optional[int]
    date_fin_effective: Optional[date]
    status: Optional[str]
    suspendu: bool
    visible: bool
    valide: bool
    intervenants: List[Intervenant] = []
    candidats: List[Candidature] = []
    historiques: List[Historique] = []
    fichiers: List[Fichier] = []
    comments: List[Commentaire] = []
    taches: List[Tache] = []

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict  # seulement necessaire avec peewee
# ============ Projet classes end ============ #


# ============ Categorie classes start ============ #
class CategorieBase(BaseModel):
    nom: Optional[str] = None
    image_url: Optional[str] = None


class CategorieCreate(CategorieBase):
    nom: str
    image_url: str

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict


class Categorie(HistoriqueBase):
    id: Optional[int] = None
    tache_id: Optional[int] = None
    projets: Optional[List[Projet]] = None

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict  # seulement necessaire avec peewee
# ============ Categorie classes end ============ #


# ============User classes start ============ #
""" Lot de classe gerant l'enregistrement et l'affichage des utilsateurs"""


class UserBase(BaseModel):
    email: str
    username: str
    nom: str
    avatar: Optional[str] = None  # url


# class UserCreate(UserBase):
#     password: str
#     role: str


class ConsultantCreate(UserBase):
    password: str
    competences: List[str] = None
    role: str = "Consultant"


class EntrepriseCreate(UserBase):
    password: str
    role: str = "Entreprise"


class Consultant(UserBase):
    id: int
    competences: Optional[List[str]] = []
    projet_diriges: List[Projet] = []
    mes_projets: List[Projet] = []
    candidatures: List[Candidature] = []
    taches: List[Tache] = []
    role: str
    is_admin: bool
    is_active: bool

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict  # seulement necessaire avec peewee


class Entreprise(UserBase):
    id: int
    offres: List[Projet] = []
    is_admin: bool
    is_active: bool
    role: str

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict  # seulement necessaire avec peewee


class User(BaseModel):
    email: Optional[str] = None
    username: Optional[str] = None
    nom: Optional[str] = None
    avatar: Optional[str] = None  # url
    is_admin: Optional[bool] = None
    is_active: Optional[bool] = None
    competences: Optional[List[str]] = None

    class Config():
        orm_mode = True
        getter_dict = PeeweeGetterDict  # seulement necessaire avec peewee
# ============ User classes end ============ #
